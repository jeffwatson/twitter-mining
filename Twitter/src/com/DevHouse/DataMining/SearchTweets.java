package com.DevHouse.DataMining;

import twitter4j.*;

import java.util.List;

public class SearchTweets {
    /**
     * Usage: java twitter4j.examples.search.com.DevHouse.DataMining.SearchTweets [query]
     * 
     * @param args
     */
    public static void main(String[] args) {
        Twitter twitter = new TwitterFactory().getInstance();
        try {
            Query query = new Query("#americanairlines");
            QueryResult result;
            do {
                result = twitter.search(query);
                List<Status> tweets = result.getTweets();
                for (Status tweet : tweets) {
                    System.out.println("@" + tweet.getUser().getScreenName() + " - "+ tweet.getUser().getCreatedAt() + " - " + tweet.getUser().getName() + " - " + tweet.getText());
                }
            } while ((query = result.nextQuery()) != null);
            System.exit(0);
        } catch (TwitterException te) {
            te.printStackTrace();
            System.out.println("Failed to search tweets: " + te.getMessage());
            System.exit(-1);
        }
    }
}

