<?php
 
 //$server = 'localhost'
 //$userName = the user to log into the database with
 //$password = the database account password
 //$databaseName = the name of the database to pull data from
 // table structure - colum1 is cas: has text/description - column2 is data has the value
 
$con = mysql_connect('localhost', 'root', 'passw0rd') or die('Error connecting to server');
 
mysql_select_db('twitter', $con); 

$file = 'file.json';

// write your SQL query here (you may use parameters from $_GET or $_POST if you need them)
$query = mysql_query('SELECT * FROM sample_data');

//Define columns 
$table = array();
$table['cols'] = array(
    array('label' => 'Username', 'type' => 'string'),
	array('label' => 'Retweet_count', 'type' => 'number'),
	array('label' => 'Origin', 'type' => 'string'),
	array('label' => 'Destination', 'type' => 'string'),
	array('label' => 'Text', 'type' => 'string'),
	array('label' => 'Sentiment_type', 'type' => 'string'),
	array('label' => 'Sentiment_score', 'type' => 'number'),
	array('label' => 'Profile_picture', 'type' => 'string'),
	array('label' => 'TweetID', 'type' => 'number')
);

$rows = array();
while($r = mysql_fetch_assoc($query)) {
    $temp = array();
	// each column needs to have data inserted via the $temp array
	$temp[] = array('v' => $r['Username']);
	$temp[] = array('v' => (int) $r['Retweet_count']); // typecast all numbers to the appropriate type (int or float) as needed - otherwise they are input as strings
	$temp[] = array('v' => $r['Origin']);
	$temp[] = array('v' => $r['Destination']);
	$temp[] = array('v' => $r['Text']);
	$temp[] = array('v' => $r['Sentiment_type']);
	$temp[] = array('v' => (double) $r['Sentiment_score']);
	$temp[] = array('v' => $r['Profile_picture']);
	$temp[] = array('v' => (int) $r['TweetID']);
	
	// insert the temp array into $rows
    $rows[] = array('c' => $temp);
}

// populate the table with rows of data
$table['rows'] = $rows;

file_put_contents($file, "");
file_put_contents($file, json_encode($table, JSON_FORCE_OBJECT), FILE_APPEND | LOCK_EX);

// Read it back out with
echo file_get_contents($file);


// set up header; first two prevent IE from caching queries
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Content-type: application/json');
?>