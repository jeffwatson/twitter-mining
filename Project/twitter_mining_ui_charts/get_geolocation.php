<?php

$con = mysql_connect('localhost', 'root', 'passw0rd') or die('Error connecting to server');

mysql_select_db('twitter', $con);

// write your SQL query here (you may use parameters from $_GET or $_POST if you need them)
$query = mysql_query('SELECT latitude, longitude, Sentiment_type, Sentiment_score
					  FROM sample_data');

//$file = 'geolocation_data.json';
$table = array();
$table['cols'] = array(
		array('label' => 'latitude', 'type' => 'number'),
		array('label' => 'longitude', 'type' => 'number'),
		array('label' => 'Sentiment_type', 'type' => 'string'),
		array('label' => 'Sentiment_score', 'type' => 'number')

);

$rows = array();
while($r = mysql_fetch_assoc($query)) {
	$temp = array();
	// each column needs to have data inserted via the $temp array
	$temp[] = array('v' => (float) $r['latitude']); // typecast all numbers to the appropriate type (int or float) as needed - otherwise they are input as strings
	$temp[] = array('v' => (float) $r['longitude']);
	$temp[] = array('v' => $r['Sentiment_type']);
	$temp[] = array('v' => (float) $r['Sentiment_score']);


	
	// insert the temp array into $rows
	$rows[] = array('c' => $temp);
}

// populate the table with rows of data
$table['rows'] = $rows;

// encode the table as JSON
$jsonTable = json_encode($table);

//Wipe file
//file_put_contents($file, "");

//Read into JSON file
//file_put_contents($file, json_encode($table, JSON_FORCE_OBJECT), FILE_APPEND | LOCK_EX);

// set up header; first two prevent IE from caching queries
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Content-type: application/json');

// return the JSON data
echo $jsonTable;
?>