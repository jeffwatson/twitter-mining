<!DOCTYPE html>
<html>
<head>
	<title>My realtime chart</title>
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
		    <script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
    		<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
			<script type="text/javascript">
			// Load the Visualization API and the piechart package.
			google.load('visualization', '1', {'packages':['corechart']});

			// Set a callback to run when the Google Visualization API is loaded.
			google.setOnLoadCallback(drawChart);

			function drawChart() {
				var json = $.ajax({
					url: 'get_sentiment_data.php', // make this url point to the data file
					dataType: 'json',
					async: false
				}).responseText;
				
				// Create our data table out of JSON data loaded from server.
				var data = new google.visualization.DataTable(json);
				var options = {
					title: 'Sentiment Prevalance (American Airlines)',
					is3D: 'false',
					width: 800,
					height: 600
				};
				// Instantiate and draw our chart, passing in some options.
				//do not forget to check ur div ID
				var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
				chart.draw(data, options);

				setInterval(drawChart, 500 );
			}
		</script>  

</head>

<body>

	  
	<div id="chart_div" style="width: 500px; height: 500px;"></div>
 
 
</body>
</html>