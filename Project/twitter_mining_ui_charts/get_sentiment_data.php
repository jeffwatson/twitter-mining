<?php
 
 //$server = 'localhost'
 //$userName = the user to log into the database with
 //$password = the database account password
 //$databaseName = the name of the database to pull data from
 // table structure - colum1 is cas: has text/description - column2 is data has the value
 
$con = mysql_connect('localhost', 'root', 'passw0rd') or die('Error connecting to server');
 
mysql_select_db('twitter', $con); 

// write your SQL query here (you may use parameters from $_GET or $_POST if you need them)
$query = mysql_query('SELECT Sentiment_type, count(*) 
					  FROM sample_data
					  GROUP BY Sentiment_type');

$file = 'sentiment_data.json';
$table = array();
$table['cols'] = array(
	/* define your DataTable columns here
	 * each column gets its own array
	 * syntax of the arrays is:
	 * label => column label
	 * type => data type of column (string, number, date, datetime, boolean)
	 */
	// I assumed your first column is a "string" type
	// and your second column is a "number" type
	// but you can change them if they are not
    array('label' => 'Sentiment_type', 'type' => 'string'),
	array('label' => 'count(*)', 'type' => 'number')
);

$rows = array();
while($r = mysql_fetch_assoc($query)) {
    $temp = array();
	// each column needs to have data inserted via the $temp array
	$temp[] = array('v' => $r['Sentiment_type']);
	$temp[] = array('v' => (int) $r['count(*)']); // typecast all numbers to the appropriate type (int or float) as needed - otherwise they are input as strings
	
	// insert the temp array into $rows
    $rows[] = array('c' => $temp);
}

// populate the table with rows of data
$table['rows'] = $rows;

// encode the table as JSON
$jsonTable = json_encode($table);

//Wipe file
file_put_contents($file, "");

//Read into JSON file
file_put_contents($file, json_encode($table, JSON_FORCE_OBJECT), FILE_APPEND | LOCK_EX);

// set up header; first two prevent IE from caching queries
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Content-type: application/json');

// return the JSON data
echo $jsonTable;
?>