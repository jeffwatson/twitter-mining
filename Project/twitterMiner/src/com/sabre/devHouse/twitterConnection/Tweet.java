package com.sabre.devHouse.twitterConnection;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.sabre.devHouse.geoMapper.Geocodes;
import org.apache.log4j.Logger;

/**
 * Package: com.sabre.devHouse.twitterConnection
 * Project: twitter-mining
 * <p/>
 * Author: Jeff Watson <Jeff.Watson@sabre.com>
 * Date: 7/17/13
 * Time: 4:22 PM
 */
public class Tweet {
    private static final Logger log = Logger.getRootLogger();

    private int id;
    private String metadata;// only in DB
    private Date createdAt;
    private long tweetId;
    private String idString;// only in DB
    private String text;
    private String source;
    private boolean isTruncated;
    private long inReplyToStatusID;
    private String inReplyToStatusIDStr;// only in DB
    private long inReplyToUserID;
    private String inReplyToUserIDStr; // only in DB
    private long favoriteCount;
    private boolean favorited;
    private boolean isRetweeted;
    private boolean possiblySensitive;
    private String lang;
    private boolean isRetweet;
    private String location;
    private long followerCount;
    private String name;
    private String screenName;
    private long retweetCount;
    private String pictureLink;
    private String lat;
    private String lon;


    //private Geocodes geo;//replaced with our object


    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getPictureLink() {
        return pictureLink;
    }

    public void setPictureLink(String pictureLink) {
        this.pictureLink = pictureLink;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }

    public String getCreatedAt(){
        return createdAt.toString();
    }

    public Date getCreatedAtDate() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        SimpleDateFormat sdfParse = new SimpleDateFormat("EEE MMM dd H:mm:ss Z yyyy");

        Date date = null;
        try {
            date = sdfParse.parse(createdAt);
        } catch (ParseException e) {
            log.error("Erroring parsing date." , e);
        }
        this.createdAt = date;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public long getTweetId() {
        return tweetId;
    }

    public void setTweetId(long tweetId) {
        this.tweetId = tweetId;
    }

    public String getIdString() {
        return idString;
    }

    public void setIdString(String idString) {
        this.idString = idString;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public boolean getIsTruncated() {
        return isTruncated;
    }

    public void setIsTruncated(boolean isTruncated) {
        this.isTruncated = isTruncated;
    }

    public long getInReplyToStatusID() {
        return inReplyToStatusID;
    }

    public void setInReplyToStatusID(long inReplyToStatusID) {
        this.inReplyToStatusID = inReplyToStatusID;
    }

    public String getInReplyToStatusIDStr() {
        return inReplyToStatusIDStr;
    }

    public void setInReplyToStatusIDStr(String inReplyToStatusIDStr) {
        this.inReplyToStatusIDStr = inReplyToStatusIDStr;
    }

    public long getInReplyToUserID() {
        return inReplyToUserID;
    }

    public void setInReplyToUserID(long inReplyToUserID) {
        this.inReplyToUserID = inReplyToUserID;
    }

    public String getInReplyToUserIDStr() {
        return inReplyToUserIDStr;
    }

    public void setInReplyToUserIDStr(String inReplyToUserIDStr) {
        this.inReplyToUserIDStr = inReplyToUserIDStr;
    }

    public Geocodes getGeocodes() {
        Geocodes geo = null;
        if(lat != null && lon != null) {
            geo = new Geocodes(Double.valueOf(lat), Double.valueOf(lon));
        }

        return geo;
    }

    /*public void setGeocodes(Geocodes geo) {
        this.geo = geo;
    }  */

    /*public Geocodes[][] getPlace() {
        return place;
    }

    public void setPlace(Geocodes[][] place) {
        this.place = place;
    }  */

    public long getFavoriteCount() {
        return favoriteCount;
    }

    public void setFavoriteCount(long favoriteCount) {
        this.favoriteCount = favoriteCount;
    }

    public boolean getFavorited() {
        return favorited;
    }

    public void setFavorited(boolean favorited) {
        this.favorited = favorited;
    }

    public boolean getPossiblySensitive() {
        return possiblySensitive;
    }

    public void setPossiblySensitive(boolean possiblySensitive) {
        this.possiblySensitive = possiblySensitive;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public boolean getIsRetweeted(){
    	return isRetweeted;
    }
    
    public void setIsRetweeted(boolean isRetweeted){
    	this.isRetweeted=isRetweeted;
    }

    public boolean getIsRetweet() {
        return isRetweet;
    }

    public void setIsRetweet(boolean isRetweet) {
        this.isRetweet = isRetweet;
    }

    public String getLocation(){
    	return location;
    }
    
    public void setLocation(String location){
    	this.location=location;
    }

    public long getFollowerCount(){
    	return followerCount;
    }
    
    public void setFollowerCount(long followerCount){
    	this.followerCount=followerCount;
    }

    public String getName(){
    	return name;
    }
    
    public void setName(String name){
    	this.name=name;
    }

    public String getScreenName(){
    	return screenName;
    }
    
    public void setScreenName(String screenName){
    	this.screenName=screenName;
    }

    public long getRetweetCount(){
    	return retweetCount;
    }
    
    public void setRetweetCount(long retweetCount){
    	this.retweetCount=retweetCount;
    }

    public String toString(){
    	return "Id: " + id + "\n" +  "Metadata: " + metadata + "\n" +  "CreatedAt: " + createdAt + "\n" +  "TweetId: " + tweetId + "\n" +  "IdString: " + idString + "\n" +  "Text: " + text + "\n" +  "Source: " + source + "\n" +  "IsTruncated: " + isTruncated + "\n" +  "InReplyToStatusID: " + inReplyToStatusID + "\n" +  "InReplyToStatusIDStr: " + inReplyToStatusIDStr + "\n" +  "InReplyToUserID: " + inReplyToUserID + "\n" +  "InReplyToUserIDStr: " + inReplyToUserIDStr + "\n" +  /*"Geo: " + getGeocodes().toString() + "\n" +*/  "FavoriteCount: " + favoriteCount + "\n" +  "Favorited: " + favorited + "\n" +  "IsRetweeted: " + isRetweeted + "\n" +  "PossiblySensitive: " + possiblySensitive + "\n" +  "Lang: " + lang + "\n" +  "IsRetweet: " + isRetweet + "\n" +  "Location: " + location + "\n" +  "FollowerCount: " + followerCount + "\n" +  "Name: " + name + "\n" +  "ScreenName: " + screenName + "\n" +  "RetweetCount: " + retweetCount + "\n\n";
    }
    public Tweet() {

    }
    
}
