package com.sabre.devHouse.twitterConnection;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Semaphore;

import com.sabre.devHouse.geoMapper.Geocodes;

import twitter4j.FilterQuery;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;

public class TwitterAPIStreamConnection implements TwitterConnector{
	Semaphore listMutex = new Semaphore(1,true), listHasContent = new Semaphore(0,true);
	List<Status> tweetList = new LinkedList<Status>();

	@Override
	public void connect() {
		StatusListener listener = new StatusListener() {
			@Override
			public void onStatus(Status status) {
				try {
					listMutex.acquire();
					tweetList.add(status);
					listHasContent.release();
					listMutex.release();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			@Override
			public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
			}

			@Override
			public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
			}

			@Override
			public void onScrubGeo(long userId, long upToStatusId) {
			}

			@Override
			public void onStallWarning(StallWarning warning) {
			}

			@Override
			public void onException(Exception ex) {
				ex.printStackTrace();
			}
		};

		TwitterStream twitterStream = new TwitterStreamFactory().getInstance();
		twitterStream.addListener(listener);
		String[] trackArray = "#americanairlines , American Airlines , #delta , #jetblue".split(",");
		twitterStream.filter(new FilterQuery(0, null, trackArray));

	}

	@Override
	public void start() {
		// nothing to do here

	}

	@Override
	public Tweet nextTweet() {
		Tweet tweet = new Tweet();
		Status s;
		try {
			listHasContent.acquire();
			listMutex.acquire();
			s = tweetList.remove(0);

			//create tweet  
			tweet.setMetadata(null);
			tweet.setCreatedAt(s.getCreatedAt());
			tweet.setTweetId(s.getId());
			tweet.setIdString(null);
			tweet.setText(s.getText());
			tweet.setSource(s.getSource());
			tweet.setIsTruncated(s.isTruncated());
			tweet.setInReplyToStatusID(s.getInReplyToStatusId());
			tweet.setInReplyToStatusIDStr(null);
			tweet.setInReplyToUserID(s.getInReplyToUserId());
			tweet.setInReplyToUserIDStr(null);
			if(s.getGeoLocation() != null){
				tweet.setLat(String.valueOf(s.getGeoLocation().getLatitude()));
				tweet.setLon(String.valueOf(s.getGeoLocation().getLongitude()));
			}
			else{
				tweet.setLat(null);
				tweet.setLon(null);
			}
			//tweet.setGeocodes(new Geocodes(s.getGeoLocation().getLatitude(),s.getGeoLocation().getLongitude()));
			//tweet.setPlace(null);//will take to much time to decode
			//tweet.setFavoriteCount(s.getFavoriteCount());
			tweet.setFavorited(s.isFavorited());
			//tweet.setIsRetweeted(s.isRetweeted());
			tweet.setIsRetweet(s.isRetweet());
			tweet.setPossiblySensitive(s.isPossiblySensitive());
			//tweet.setLang(s.getIsoLanguageCode());
			tweet.setLocation(s.getUser().getLocation());
			tweet.setFollowerCount(s.getUser().getFollowersCount());
			tweet.setName(s.getUser().getName());
			tweet.setScreenName(s.getUser().getScreenName());
			tweet.setRetweetCount(s.getRetweetCount());
			tweet.setPictureLink(s.getUser().getBiggerProfileImageURL());

			return tweet;
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally{
			listMutex.release();
		}
		return null;
	}

	@Override
	public boolean hasNext() {
		return true;
	}

	/*public static void main (String [] args){
		TwitterConnector tmp = new TwitterAPIStreamConnection();
		tmp.connect();
		tmp.start();
		while (tmp.hasNext()){
			Status s = tmp.nextTweet();
			System.out.println("@" + s.getUser().getScreenName() + " - " + s.getText());
		}

	} */
}
