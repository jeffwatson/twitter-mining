package com.sabre.devHouse.twitterConnection;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import twitter4j.Status;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

/**
 * Package: com.sabre.devHouse.twitterConnection
 * Project: twitter-mining
 * <p/>
 * Author: Jeff Watson <Jeff.Watson@sabre.com>
 * Date: 7/18/13
 * Time: 10:04 AM
 */
public class mJSONParser {
    private static final Logger log = Logger.getRootLogger();
    private static final String JSONFileLocation = "tweets.json";
    private static URL JSONRes;
    private static SessionFactory factory;

    public mJSONParser() {

        JSONRes = getClass().getClassLoader().getResource(JSONFileLocation);

        try{
            factory = new Configuration().configure().buildSessionFactory();
        }catch (Throwable ex) {
            System.err.println("Failed to create sessionFactory object." + ex);
            throw new ExceptionInInitializerError(ex);
        }

    }

    public void parseJSON() {
        ArrayList<Tweet> mList = new ArrayList<Tweet>();
        JSONParser parser = new JSONParser();

        File f = new File(JSONRes.getFile());

        log.info("reading from json " + f.getAbsolutePath());

        try {
            Object obj = parser.parse(new FileReader(f));
            JSONArray array = (JSONArray) obj;

            for (Object mObject : array) {
                JSONObject jsonObject = (JSONObject) mObject;

                //String metadata = (String) jsonObject.get("metadata");
                String createdAt = (String) jsonObject.get("created_at");
                SimpleDateFormat sdfParse = new SimpleDateFormat("EEE MMM dd H:mm:ss Z yyyy");
                SimpleDateFormat sdfFormat = new SimpleDateFormat("M/dd/yyyy H:mm");
                Date createdAtDate = sdfParse.parse(createdAt);
                //createdAtDate = sdfFormat.

                Long tweetId = (Long) jsonObject.get("id");
                String idStr = (String) jsonObject.get("id_str");
                String text = (String) jsonObject.get("text");
                String source = (String) jsonObject.get("source");
                Boolean truncated = (Boolean) jsonObject.get("truncated");
                Long inReplyToStatusId = (Long) jsonObject.get("in_reply_to_status_id");
                String inReplyToStatusIdStr = (String) jsonObject.get("in_reply_to_status_id_str");
                Long inReplyToUserId = (Long) jsonObject.get("in_reply_to_user_id");
                String inReplyToUserIdStr = (String) jsonObject.get("in_reply_to_user_id_str");
                Long favoriteCount = (Long) jsonObject.get("favorite_count");
                Boolean favorited = (Boolean) jsonObject.get("favorited");
                Boolean possiblySensitive = (Boolean) jsonObject.get("possibly_sensitive");
                String lang = (String) jsonObject.get("lang");

                // TODO: GEO
                // TODO: PLACE
                // ENTITIES ??

                String name = null;
                String screenName = null;
                String pictureLink = null;
                String location = null;
                Long followerCount = 0l;
                Long retweetCount = 0l;
                Boolean retweeted = false;
                String lat = null;
                String lon = null;


                // USER
                JSONObject jsonObjectUser = (JSONObject) jsonObject.get("user");
                    name = (String) jsonObjectUser.get("name");
                    screenName = (String) jsonObjectUser.get("screen_name");
                    pictureLink = (String) jsonObjectUser.get("profile_image_url");
                    location = (String) jsonObjectUser.get("location");
                    followerCount = (Long) jsonObjectUser.get("followers_count");
                    retweetCount = (Long) jsonObjectUser.get("retweet_count");
                    retweeted = (Boolean) jsonObjectUser.get("retweeted");

                // GEO
                JSONObject jsonObjectGeo = (JSONObject) jsonObject.get("geo");
                    if(jsonObjectGeo != null) {
                        JSONArray latLon = (JSONArray) jsonObjectGeo.get("coordinates");
                        if(latLon != null) {
                            System.out.println(latLon);
                            lat = latLon.get(0).toString();
                            lon = latLon.get(1).toString();
                        }
                    }



                // Make the object and add it to the list
                Tweet tweet = new Tweet();

                //tweet.setMetadata(metadata);
                //TODO convert created at into a date...
                tweet.setCreatedAt(createdAt);
                tweet.setTweetId(tweetId);
                tweet.setIdString(idStr);
                tweet.setText(text.replaceAll("[^\\x20-\\x7e]", ""));
                tweet.setSource(source);
                tweet.setIsTruncated(truncated);
                if(inReplyToStatusId != null) tweet.setInReplyToStatusID(inReplyToStatusId);
                tweet.setInReplyToStatusIDStr(inReplyToStatusIdStr);
                if(inReplyToUserId != null) tweet.setInReplyToUserID(inReplyToUserId);
                tweet.setInReplyToUserIDStr(inReplyToUserIdStr);
                tweet.setFavoriteCount(favoriteCount);
                tweet.setFavorited(favorited);
                if(possiblySensitive != null) tweet.setPossiblySensitive(possiblySensitive);
                tweet.setLang(lang);
                tweet.setPictureLink(pictureLink);
                tweet.setName(name);
                tweet.setScreenName(screenName);
                tweet.setLocation(location);
                tweet.setFollowerCount(followerCount);
                if(retweetCount != null) tweet.setRetweetCount(retweetCount);
                if(retweeted != null) tweet.setIsRetweeted(retweeted);
                if(lon != null) tweet.setLon(lon);
                if(lat != null) tweet.setLat(lat);

                mList.add(tweet);
            }

        } catch (FileNotFoundException e) {
            log.error(e.toString());
            mList = null;
        } catch (IOException e) {
            log.error(e.toString());
            mList = null;
        } catch (ParseException e) {
            log.error(e.toString());
            mList = null;
        } catch (java.text.ParseException e) {
            log.error(e.toString());
            mList = null;
        }

        //System.out.println(mList.toString());


        // save the info the db
        Session session = factory.openSession();
        Transaction tx = null;
        Integer tweetID = null;

            try{
                tx = session.beginTransaction();
                for(Tweet tweet: mList) {
                    tweetID = (Integer) session.save(tweet);
                    //System.out.println(tweetID);
                }
                tx.commit();
            }catch (HibernateException e) {
                //if (tx!=null) tx.rollback();
                log.error("Error adding to TWEETS db.", e);
            }finally {
                session.close();
            }

        //session.close();


        //if (mList != null && mList.size() == 0) {
        //    mList = null;
        //}
        //return mList;

    }
}
