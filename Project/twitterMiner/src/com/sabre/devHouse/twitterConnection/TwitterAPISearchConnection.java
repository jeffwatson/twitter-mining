package com.sabre.devHouse.twitterConnection;

import java.util.LinkedList;
import java.util.List;

import twitter4j.*;

public class TwitterAPISearchConnection implements TwitterConnector{
	private List<Status> tweetList = new LinkedList<Status>();
	private Twitter twitter;
	@Override
	public void connect() {
		Twitter twitter = new TwitterFactory().getInstance();
	}

	@Override
	public void start() {
		try {
	        Query query = new Query("#americanairlines");
	        QueryResult result;
	        do {
	            result = twitter.search(query);
	            List<Status> tweets = result.getTweets();
	            for (Status tweet : tweets) {
	                tweetList.add(tweet);
	            }
	        } while ((query = result.nextQuery()) != null);
	    } catch (TwitterException te) {
	        te.printStackTrace();
	        System.out.println("Failed to search tweets: " + te.getMessage());
	    }
	}

	@Override
	public Tweet nextTweet() {
        Tweet mTweet = new Tweet();
        Status s = tweetList.remove(0);

        // Create a TweetData Object from the Status
        mTweet.setText(s.getText());
        mTweet.setName(s.getUser().getName());
        mTweet.setCreatedAt(s.getCreatedAt());
        mTweet.setRetweetCount(s.getRetweetCount());
        return mTweet;
	}
	
	@Override
	public boolean hasNext(){
		return tweetList.size() > 0;
		
	}
}
