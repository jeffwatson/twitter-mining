package com.sabre.devHouse.twitterConnection;

public interface TwitterConnector {
	public void connect();
	public void start();
	public Tweet nextTweet();
	public boolean hasNext();
}
