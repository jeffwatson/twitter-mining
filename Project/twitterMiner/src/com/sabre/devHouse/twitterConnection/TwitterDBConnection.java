package com.sabre.devHouse.twitterConnection;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.ArrayList;
import java.util.List;

public class TwitterDBConnection implements TwitterConnector{
    private static final Logger log = Logger.getRootLogger();

    private static SessionFactory factory;
    private List<Tweet> mList = new ArrayList<Tweet>();
    private int index = 0;

    public TwitterDBConnection() {
        try{
            factory = new Configuration().configure().buildSessionFactory();
        }catch (Throwable ex) {
            log.error("Failed to Create SessionFactory object.", ex);
            throw new ExceptionInInitializerError(ex);
        }
        Session session = factory.openSession();


        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            mList = session.createQuery("FROM com.sabre.devHouse.twitterConnection.Tweet").list();

            tx.commit();
        }catch (Exception e) {
            if (tx!=null) tx.rollback();
            log.error("Failed to commit changes.", e);
        }finally {
            session.close();
        }

        //System.out.println(mList.toString());
    }

	@Override
	public void connect() {
		// this is a dummy method
		
	}

	@Override
	public void start() {
		//this is a dummy method
		
	}

	@Override
	public Tweet nextTweet() {
		Tweet mTweet = mList.get(index);
        index++;

        return mTweet;
        //return null;
	}

	@Override
	public boolean hasNext() {
		return index < mList.size();
	}

    public List list() {
        return mList;
    }

}
