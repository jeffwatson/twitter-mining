package com.sabre.devHouse.twitterConnection;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Semaphore;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class DemoConnection implements TwitterConnector {
	Semaphore listMutex = new Semaphore(1,true), listHasContent = new Semaphore(0,true);
	List<Tweet> tweetList = new LinkedList<Tweet>();
	Window window;
	@Override
	public void connect() {
		// TODO Auto-generated method stub

	}

	@Override
	public void start() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					window = new Window(listMutex,listHasContent,tweetList);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	@Override
	public Tweet nextTweet() {
		try{
			listHasContent.acquire();
			listMutex.acquire();
			Tweet tweet = tweetList.remove(0);
			return tweet;
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally{
			listMutex.release();
		}
		return null;
	}

	@Override
	public boolean hasNext() {
		// TODO Auto-generated method stub
		return true;
	}
	private class Window {

		private JFrame frame;
		public JTextField textField;
		public JTextField textField_1;
		public JTextField textField_2;
		public JTextField textField_3;
		public JTextField textField_4;
		private JLabel lblMmddyyHhmm;

		Semaphore listMutex, listHasContent;
		List<Tweet> tweetList;

		public Window(Semaphore listMutex, Semaphore listHasContent,
				List<Tweet> tweetList) {
			this.listMutex= listMutex;
			this.listHasContent = listHasContent;
			this.tweetList = tweetList;
			initialize();
		}

		/**
		 * Create the application.
		 */
		public Window() {
			initialize();
		}

		/**
		 * Initialize the contents of the frame.
		 */
		private void initialize() {
			frame = new JFrame();
			frame.setBounds(100, 100, 450, 300);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.getContentPane().setLayout(null);

			JLabel lblTweet = new JLabel("Tweet");
			lblTweet.setBounds(12, 13, 42, 22);
			frame.getContentPane().add(lblTweet);

			textField = new JTextField();
			textField.setBounds(12, 36, 352, 90);
			frame.getContentPane().add(textField);
			textField.setColumns(10);

			JButton btnSubmit = new JButton("Submit");
			btnSubmit.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy HH:mm");
					Date timestamp = null;
					double latitude = 0,longitude = 0;
					String tweetTxt = textField.getText();
					if(tweetTxt.isEmpty()){
						return;
					}
					try{
						timestamp = sdf.parse(textField_3.getText());
						latitude=Double.parseDouble(textField_1.getText());
						longitude=Double.parseDouble(textField_2.getText());

					}catch(Exception e){
					}

					Tweet tweet = new Tweet();
					//create tweet  
					tweet.setMetadata(null);
					tweet.setCreatedAt(timestamp);//////////
					tweet.setTweetId(1);//////////
					tweet.setIdString(null);
					tweet.setText(tweetTxt);//////////
					tweet.setSource(null);
					tweet.setIsTruncated(false);
					tweet.setInReplyToStatusID(-1);
					tweet.setInReplyToStatusIDStr(null);
					tweet.setInReplyToUserID(-1);
					tweet.setInReplyToUserIDStr(null);
					tweet.setLat(""+latitude);//////////
					tweet.setLon(""+longitude);//////////
					tweet.setFavorited(true);
					tweet.setIsRetweet(false);
					tweet.setPossiblySensitive(false);
					tweet.setLocation("");
					tweet.setFollowerCount(Long.MAX_VALUE);
					tweet.setName("Mr. Demo");
					tweet.setScreenName("TwitterMiner APP");
					tweet.setRetweetCount(Long.MAX_VALUE);
					tweet.setPictureLink("localhost:8080");

					try {
						listMutex.acquire();
						tweetList.add(tweet);
						listHasContent.release();
						listMutex.release();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			});
			btnSubmit.setBounds(323, 217, 97, 25);
			frame.getContentPane().add(btnSubmit);

			JLabel lblLatitude = new JLabel("Latitude");
			lblLatitude.setBounds(12, 139, 56, 16);
			frame.getContentPane().add(lblLatitude);

			textField_1 = new JTextField();
			textField_1.setBounds(65, 136, 116, 22);
			frame.getContentPane().add(textField_1);
			textField_1.setColumns(10);

			JLabel lblLongitude = new JLabel("Longitude");
			lblLongitude.setBounds(190, 139, 56, 16);
			frame.getContentPane().add(lblLongitude);

			textField_2 = new JTextField();
			textField_2.setBounds(258, 136, 116, 22);
			frame.getContentPane().add(textField_2);
			textField_2.setColumns(10);

			JLabel lblDate = new JLabel("Date Time");
			lblDate.setBounds(12, 168, 64, 16);
			frame.getContentPane().add(lblDate);

			textField_3 = new JTextField();
			textField_3.setBounds(75, 165, 136, 22);
			frame.getContentPane().add(textField_3);
			textField_3.setColumns(10);

			lblMmddyyHhmm = new JLabel("MM/DD/YY HH:MM");
			lblMmddyyHhmm.setBounds(85, 188, 116, 16);
			frame.getContentPane().add(lblMmddyyHhmm);
		}
	}
	public static void main(String [] args){
		DemoConnection tmp = new DemoConnection();
		tmp.start();
	}
}
