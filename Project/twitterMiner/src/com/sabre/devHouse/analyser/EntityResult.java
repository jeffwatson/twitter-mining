package com.sabre.devHouse.analyser;

public class EntityResult {
	private String entity;
	private double relevance;
	public EntityResult(String entity, double relevance){
		this.entity = entity;
		this.relevance = relevance;
	}
	public void setRelevance(double relevance){
		this.relevance = relevance;
	}
	public double getRelevance(){
		return relevance;
	}
	public void setEntity(String entity){
		this.entity = entity;
	}
	public String getEntity(){
		return entity;
	}
	public String toString(){
		return "Entity: " + entity + " Relevance: " + relevance;
	}
	public EntityResult(){
		
	}
}
