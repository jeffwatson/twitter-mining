package com.sabre.devHouse.analyser;

import java.util.List;

public interface AnalyserConnector {
	public SentimentResult analyseForSentiment(String text);
	public SentimentResult analyseForSentiment(String text, String entity);
	public List<EntityResult> analyseForEntity(String text);
}
