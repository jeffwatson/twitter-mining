package com.sabre.devHouse.analyser;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.alchemyapi.api.AlchemyAPI;
import com.alchemyapi.api.AlchemyAPI_TargetedSentimentParams;
import com.sabre.devHouse.util.Constants;

public class AlchemyConnection implements AnalyserConnector{
    AlchemyAPI alchemyObj;
    private static final Logger log = Logger.getRootLogger();

    public AlchemyConnection() {
        System.setProperty("http.proxyHost", Constants.PROXY);
        System.setProperty("http.proxyPort", Integer.toString(Constants.PROXY_PORT));

        try {
            alchemyObj = AlchemyAPI.GetInstanceFromString(Constants.ALCHEMY_API_KEY);
        } catch (Exception e) {
            System.out.println("Unable to initiate Alchemy api.");
            e.printStackTrace();
        }
    }

	@Override
	public SentimentResult analyseForSentiment(String text) {
		SentimentResult result = new SentimentResult();
        Document doc;
        Element element;
        String scoreString = "0.0";
        String typeString = "neutral";
        Double scoreValue = 0.0;

        try {
            doc = alchemyObj.TextGetTextSentiment(text);
            element = doc.getDocumentElement();

            // Get the elements from the xml
            // they're the only of their kind, therefore use item 0
            typeString = element.getElementsByTagName("type").item(0).getFirstChild().getNodeValue();
            scoreString = element.getElementsByTagName("score").item(0).getFirstChild().getNodeValue();

            scoreValue = Double.parseDouble(scoreString);
        } catch (Exception e) {
            log.error("failure in Alchemy connection.", e);
        }

        result = new SentimentResult(typeString, scoreValue);

		return result;
	}

	@Override
	public SentimentResult analyseForSentiment(String text, String entity) {
		SentimentResult result = new SentimentResult();
        Document doc;
        Element element;
        String scoreString = "0.0";
        String typeString = "neutral";
        Double scoreValue = 0.0;
        if (text.length() < 5 || entity.length() < 5){
        	log.info("text or entity too short!");
        	return new SentimentResult(typeString, scoreValue);
        }
        try {
        	AlchemyAPI_TargetedSentimentParams sentimentParams = new AlchemyAPI_TargetedSentimentParams();
            sentimentParams.setShowSourceText(true);
        	doc = alchemyObj.TextGetTargetedSentiment(text, entity, sentimentParams);
            element = doc.getDocumentElement();

            // Get the elements from the xml
            // they're the only of their kind, therefore use item 0
            typeString = element.getElementsByTagName("type").item(0).getFirstChild().getNodeValue();
            scoreString = element.getElementsByTagName("score").item(0).getFirstChild().getNodeValue();

            scoreValue = Double.parseDouble(scoreString);
        } catch (Exception e) {
            log.error("failure in Alchemy connection.", e);
        }

        result = new SentimentResult(typeString, scoreValue);

		return result;
	}

	@Override
	public List<EntityResult> analyseForEntity(String text) {
		List<EntityResult> toReturn = new ArrayList<EntityResult>();
        Document doc;
        Element element;

        try {
        	doc = alchemyObj.TextGetRankedNamedEntities(text);
            element = doc.getDocumentElement();
            element = (Element)doc.getElementsByTagName("entities").item(0);
            NodeList nList = doc.getElementsByTagName("entity");
			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				Element eElement = (Element) nNode;
				if(eElement.getElementsByTagName("type").item(0).getTextContent().equals("Company")){
					String name = eElement.getElementsByTagName("text").item(0).getTextContent();
					double relevance = Double.parseDouble(eElement.getElementsByTagName("relevance").item(0).getTextContent());
					toReturn.add(new EntityResult(name,relevance));
				}
			}
        } catch (Exception e) {
            log.error("failure in Alchemy connection.", e);
        }

		return toReturn;
	}
	public static void main (String [] args){
		AlchemyConnection tmp = new AlchemyConnection();
		//System.out.print(tmp.analyseForSentiment("This airplane is terrible.", "airplane"));
		System.out.println(tmp.analyseForEntity("American Airlines has run out of overhead space and is willing to check our carry-on for \"no charge.\" Thanks guys. @ Chicago - O'Hare"));
	}
}
