package com.sabre.devHouse.analyser;

public class SentimentResult {
    private String type;
    private double score;

    public SentimentResult() {
    }

    public SentimentResult(String t, double s) {
        this.type = t;
        this.score = s;
    }

    public String getType() {
        return this.type;
    }

    public double getScore() {
        return this.score;
    }

    @Override
    public String toString() {
        return "type: " + this.type + ", score: " + this.score;
    }
}
