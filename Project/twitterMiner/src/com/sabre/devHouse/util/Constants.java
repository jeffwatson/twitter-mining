package com.sabre.devHouse.util;

public class Constants {
	public static final String PROXY = "www-ad-proxy.sabre.com";
	public static final int PROXY_PORT = 80;
	public static final String AIRPORT_DATA_FILE_PATH = "airports.dat";
	public static final String AIRLINE_DATA_FILE_PATH = "airlines.dat";
    public static final String ALCHEMY_API_KEY = "d73c10d0ab430f7bc7f549fd074577f67db3bd86";
    public static final boolean enableLocationByEntityDetection = false;
    public static final boolean enableLocationByTweetLocation = false;
    public static final long TIME_OF_TWEET_THRESHOLD = 1000 * 60 * 60 * 2; // 1 hour buffer
    public static final int EUCLEDIAN_THRESHOLD = 2; //1 degree ~ 69 mi latitude or [0, 69] mi longitude 0 at pole
    public static final int LEVENSHTEIN_THRESHOLD = 13;//distance between two strings
    public static final String TWITTER_API_KEYWORDS= "#AmericanAirlines|#Delta";
}
