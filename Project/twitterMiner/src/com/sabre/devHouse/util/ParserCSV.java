package com.sabre.devHouse.util;

import java.util.ArrayList;
import java.util.List;


public class ParserCSV {
	public ParserCSV(){
	}
	public List<String> parseCommaVSLine(String line){
		if(line == null || line == ""){
			return new ArrayList<String>();
		}
		ArrayList<String> list = new ArrayList<String>();
		StringBuilder element = new StringBuilder();
		boolean inQuotes = false;
		char comma = ',', quote = '"';
		for(int i = 0; i < line.length();i++){
			char character = line.charAt(i);
			if ( character == comma && !inQuotes){
				list.add(element.toString());
				element = new StringBuilder();
			} else if ( character == quote && !inQuotes){
				inQuotes = true;
			} else if ( character == quote && ((i < line.length() - 1 && line.charAt(i+1) == quote ) || i == line.length() - 1) && inQuotes){
				element.append(quote);
				i++;
			} else if ( character == quote && line.charAt(i+1) == comma && inQuotes){
				inQuotes = false;
			} else if ( (character != comma & !inQuotes) 
					|| inQuotes ){
				element.append(character);
			}
		}
		list.add(element.toString());
		return list;
	}
}