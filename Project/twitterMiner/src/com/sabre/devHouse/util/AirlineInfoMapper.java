package com.sabre.devHouse.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AirlineInfoMapper {
	Map<String, String> mapICAOtoAirline = new HashMap<String, String>();
    public static URL AirlinesRes;

	public AirlineInfoMapper(){
		ParserCSV parser = new ParserCSV();
		BufferedReader input;
		try{
            AirlinesRes = getClass().getClassLoader().getResource(Constants.AIRLINE_DATA_FILE_PATH);
            File f = new File(AirlinesRes.getFile());

			input = new BufferedReader(new FileReader(f));
			String line;
			while((line = input.readLine()) != null){
				List<String> elems = parser.parseCommaVSLine(line);
				mapICAOtoAirline.put(elems.get(4), elems.get(1));

			}
			input.close();
		}catch(IOException ioe){
			ioe.printStackTrace();
		} 
	}
	public String getAirlineOfICAO(String ICAO) {
		return mapICAOtoAirline.get(ICAO);
	}
	public static void main (String [] args){
		AirlineInfoMapper tmp = new AirlineInfoMapper();
		System.out.println(tmp.getAirlineOfICAO("AAL"));
	}
}
