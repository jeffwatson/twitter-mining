package com.sabre.devHouse.twitterMiner;

import com.sabre.devHouse.analyser.EntityResult;
import com.sabre.devHouse.analyser.SentimentResult;
import com.sabre.devHouse.flightDataConnection.FlightLeg;
import com.sabre.devHouse.twitterConnection.Tweet;

/**
 * Package: com.sabre.devHouse.twitterMiner
 * Project: twitter-mining
 * <p/>
 * Author: Jeff Watson <Jeff.Watson@sabre.com>
 * Date: 7/17/13
 * Time: 11:16 AM
 */
public class MatchedTweet {
	private int id;
	private String tweetId;
	private String userName;//private String name;
	private String screenName;
	private String tweetText;//private String text;
	private String tweetTime;//private Date createdAt;
	private String favoriteCount;
	private String followerCount;
	private String retweetCount = "0.0";
	private String pictureLink;

	private String departureTime;
	private String arrivalTime;
	private String originCity;
	private String originAirport;//private String OriginName
	private String destinationAirport;//private String DestinationName;
	private String destinationCity;

	private String sentimentType;
	private String sentimentValue;

	private String airlineName;
	private String airlineRelevance;

	public MatchedTweet() {

	}

	/** Use this constructor!
	 *
	 * @param status Status object representing a tweet
	 * @param flight FlightLeg object showing which flight the user is/was on
	 * @param sentiment SentimentResponse showing how the user felt about the flight.
	 */
	public MatchedTweet(Tweet status, EntityResult entities, FlightLeg flight, SentimentResult sentiment) {

		this.tweetId = ""+status.getTweetId();
		this.userName =status.getName();
		this.screenName =status.getScreenName();
		this.tweetText =status.getText();
		this.tweetTime =status.getCreatedAt();
		this.favoriteCount =""+status.getFavoriteCount();
		this.followerCount =""+status.getFollowerCount();
		this.retweetCount =""+status.getRetweetCount();
		this.pictureLink =status.getPictureLink();

        if(flight != null) {
		    this.departureTime =flight.getDepartureTime();
		    this.arrivalTime =flight.getArrivalTime();
		    this.originCity =flight.getOriginCity();
		    this.originAirport =flight.getOriginName();
		    this.destinationAirport =flight.getDestinationName();
		    this.destinationCity =flight.getDestinationCity();
        }
		this.sentimentType =sentiment.getType();
		this.sentimentValue =""+sentiment.getScore();
		this.airlineName =entities.getEntity();
		this.airlineRelevance =""+entities.getRelevance();
	}

	/**
	 * Don't use this constructor! it's for testing purposes only.
	 *
	 * @param t tweet status
	 * @param u username
	 * @param ti time
	 * @param rt retweet count
	 * @param url link to user's profile picture
	 * @param flight
	 * @param sentiment
	 */

	public MatchedTweet (String t, String u, String ti, String rt, String url, FlightLeg flight, SentimentResult sentiment) {
		tweetText = t;
		userName = u;
		tweetTime = ti;
		originAirport = flight.getOriginCity();
		destinationAirport = flight.getDestinationCity();
		retweetCount = rt;
		sentimentType = sentiment.getType();
		sentimentValue = String.valueOf(sentiment.getScore());
		pictureLink = url;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTweetId() {
		return tweetId;
	}

	public void setTweetId(String tweetId) {
		this.tweetId = tweetId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getScreenName() {
		return screenName;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	public String getTweetText() {
		return tweetText;
	}

	public void setTweetText(String tweetText) {
		this.tweetText = tweetText;
	}

	public String getTweetTime() {
		return tweetTime;
	}

	public void setTweetTime(String tweetTime) {
		this.tweetTime = tweetTime;
	}

	public String getFavoriteCount() {
		return favoriteCount;
	}

	public void setFavoriteCount(String favoriteCount) {
		this.favoriteCount = favoriteCount;
	}

	public String getFollowerCount() {
		return followerCount;
	}

	public void setFollowerCount(String followerCount) {
		this.followerCount = followerCount;
	}

	public String getRetweetCount() {
		return retweetCount;
	}

	public void setRetweetCount(String retweetCount) {
		this.retweetCount = retweetCount;
	}

	public String getPictureLink() {
		return pictureLink;
	}

	public void setPictureLink(String pictureLink) {
		this.pictureLink = pictureLink;
	}

	public String getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

	public String getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public String getOriginCity() {
		return originCity;
	}

	public void setOriginCity(String originCity) {
		this.originCity = originCity;
	}

	public String getOriginAirport() {
		return originAirport;
	}

	public void setOriginAirport(String originAirport) {
		this.originAirport = originAirport;
	}

	public String getDestinationAirport() {
		return destinationAirport;
	}

	public void setDestinationAirport(String destinationAirport) {
		this.destinationAirport = destinationAirport;
	}

	public String getDestinationCity() {
		return destinationCity;
	}

	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}

	public String getSentimentType() {
		return sentimentType;
	}

	public void setSentimentType(String sentimentType) {
		this.sentimentType = sentimentType;
	}

	public String getSentimentValue() {
		return sentimentValue;
	}

	public void setSentimentValue(String sentimentValue) {
		this.sentimentValue = sentimentValue;
	}

	public String getAirlineName() {
		return airlineName;
	}

	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}

	public String getAirlineRelevance() {
		return airlineRelevance;
	}

	public void setAirlineRelevance(String airlineRelevance) {
		this.airlineRelevance = airlineRelevance;
	}

}
