package com.sabre.devHouse.twitterMiner;

/**
 * Package: com.sabre.devHouse.twitterMiner
 * Project: twitter-mining
 * <p/>
 * Author: Jeff Watson <Jeff.Watson@sabre.com>
 * Date: 7/16/13
 * Time: 4:26 PM
 */
public interface MatchedTweetsConnector {
    public boolean addMatchedTweet(MatchedTweet tweet);
}
