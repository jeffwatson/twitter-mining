package com.sabre.devHouse.twitterMiner;

import com.sabre.devHouse.flightDataConnection.FlightLeg;
import com.sabre.devHouse.analyser.SentimentResult;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;
import twitter4j.Status;

import java.util.ArrayList;
import java.util.List;

/**
 * Package: com.sabre.devHouse.twitterMiner
 * Project: twitter-mining
 * <p/>
 * Author: Jeff Watson <Jeff.Watson@sabre.com>
 * Date: 7/16/13
 * Time: 4:37 PM
 */
public class MatchedTweetsDBConnection implements MatchedTweetsConnector{
    private static final Logger log = Logger.getRootLogger();
    private static SessionFactory factory;

    public MatchedTweetsDBConnection() {
        try{
            factory = new Configuration().configure().buildSessionFactory();
        }catch (Throwable ex) {
            System.err.println("Failed to create sessionFactory object." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public List<MatchedTweet> getAllTweets() {
        List mList = new ArrayList<MatchedTweet>();

        Session session = factory.openSession();
        Transaction tx = null;
        Integer tweetID = null;
        try{
            tx = session.beginTransaction();
            mList = session.createQuery("FROM com.sabre.devHouse.twitterMiner.MatchedTweet").list();
            //tweetID = (Integer) session.save(tweet);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            log.error("Error adding to MATCHED TWEETS db.", e);
        }finally {
            session.close();
        }

        return mList;

    }

    /*
     * returns true if the status and flight were successfully added to the database.
     */
    @Override
    public boolean addMatchedTweet(MatchedTweet tweet) {

        Session session = factory.openSession();
        Transaction tx = null;
        Integer tweetID = null;
        try{
            tx = session.beginTransaction();
            tweetID = (Integer) session.save(tweet);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            log.error("Error adding to MATCHED TWEETS db.", e);
        }finally {
            session.close();
        }

        System.out.println(tweetID);

        return tweetID == null;
    }
}
