package com.sabre.devHouse.twitterMiner;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.sabre.devHouse.analyser.SentimentResult;
import com.sabre.devHouse.twitterConnection.Tweet;
import org.apache.log4j.Logger;

import com.sabre.devHouse.analyser.AlchemyConnection;
import com.sabre.devHouse.analyser.AnalyserConnector;
import com.sabre.devHouse.analyser.EntityResult;
import com.sabre.devHouse.flightDataConnection.FlightDBConnection;
import com.sabre.devHouse.flightDataConnection.FlightDataConnector;
import com.sabre.devHouse.flightDataConnection.FlightLeg;
import com.sabre.devHouse.geoMapper.Geocodes;
import com.sabre.devHouse.geoMapper.GeocodesMapper;
import com.sabre.devHouse.geoMapper.GoogleAPIConnection;
import com.sabre.devHouse.twitterConnection.DemoConnection;
import com.sabre.devHouse.twitterConnection.TwitterAPIStreamConnection;
import com.sabre.devHouse.twitterConnection.TwitterConnector;
import com.sabre.devHouse.twitterConnection.TwitterDBConnection;
import com.sabre.devHouse.util.AirlineInfoMapper;
import com.sabre.devHouse.util.Constants;

public class TwitterMiner {
	private static final Logger log = Logger.getRootLogger();

	TwitterConnector connectionToTweets;
	FlightDataConnector connectionToFlightData;
	GeocodesMapper geoMapper;
	AnalyserConnector analyser;
	MatchedTweetsConnector tweetRecorder;
	AirlineInfoMapper airlineMapper;

	public static void main (String [] args){
		TwitterMiner tm = new TwitterMiner();
		System.out.println("STARTING APPLICATION");
		tm.start();
	}
	public TwitterMiner(){
		connectionToTweets = new DemoConnection();
		connectionToFlightData = new FlightDBConnection();
		geoMapper = new GoogleAPIConnection();
		analyser = new AlchemyConnection();
		tweetRecorder = new MatchedTweetsDBConnection();
		airlineMapper = new AirlineInfoMapper();
	}

	public void start(){
		connectionToTweets.connect();
		connectionToTweets.start();

		while (connectionToTweets.hasNext()){
			Tweet tweet = connectionToTweets.nextTweet();
			//Get Geocodes
			Geocodes locationOfTweet = null;
			boolean candidate = false;
			if (tweet.getGeocodes() != null){
				locationOfTweet = tweet.getGeocodes();
				candidate = true;
			} else if (Constants.enableLocationByEntityDetection){

			} else if (Constants.enableLocationByTweetLocation){

			}

			if (candidate){
				List<EntityResult> entities = analyser.analyseForEntity(tweet.getText());
				System.out.println(entities);
				if(entities == null || entities.isEmpty()){
					continue;
				}
				Map<String,Geocodes> nearbyAirports = geoMapper.getAirportsNear(locationOfTweet);
				SentimentResult result = analyser.analyseForSentiment(tweet.getText());
				List<FlightLeg> potentialFlights = new ArrayList<FlightLeg>();
				connectionToFlightData.getPossibleFlights(tweet.getCreatedAtDate(), potentialFlights);

				log.info("User:" + tweet.getName());
				log.info("Date" + tweet.getCreatedAt());
				log.info("Tweet: " + tweet.getText());
				log.info("Location: " + tweet.getGeocodes());
				log.info("Airline: " + entities);
				log.info("Nearby Airports: " + nearbyAirports.keySet());
				log.info("Sentiment results: " + result);
				log.info("Potential fligths after timestamp filter: " + potentialFlights);
                System.out.println(potentialFlights.size());
				potentialFlights = filterPotentialFlightsByAirports(potentialFlights, nearbyAirports);
                System.out.println(potentialFlights);
				log.info("Potential fligths after nearby filter: " + potentialFlights);
				
				//potentialFlights = filterPotentialFlightsByAirlineName(potentialFlights, entities.get(0));
				
				log.info("Potential fligths after airline name filter: " + potentialFlights);
                System.out.println(potentialFlights);

				//if(potentialFlights.size()>0){    //MR. DEMO
					//MatchedTweet record = new MatchedTweet(tweet, entities.get(0), potentialFlights.get(0), result);
                    MatchedTweet record = new MatchedTweet(tweet, entities.get(0), potentialFlights.get(0), result);
                    System.out.println("Adding tweet. Mr. Demo " + record.getScreenName());
					tweetRecorder.addMatchedTweet(record);
				//}
			}
		}
		System.out.println("PROCESSING COMPLETE");
	}
	private List<FlightLeg> filterPotentialFlightsByAirlineName(
			List<FlightLeg> potentialFlights, EntityResult entityResult) {
		List<FlightLeg> toReturn = new ArrayList<FlightLeg>();
		for (FlightLeg flight: potentialFlights){
			String flightAirline = airlineMapper.getAirlineOfICAO(flight.getIdent().substring(0,3));
			if (getLevenshteinDistance(entityResult.getEntity(), flightAirline) <= Constants.LEVENSHTEIN_THRESHOLD){
				toReturn.add(flight);
			}
		}
		return toReturn;
	}
	private List<FlightLeg> filterPotentialFlightsByAirports(
			List<FlightLeg> potentialFlights,
			Map<String, Geocodes> nearbyAirports) {
		List<FlightLeg> flightsInRange = new ArrayList<FlightLeg>();
		for (FlightLeg flight: potentialFlights){
			Geocodes originGeocodes = geoMapper.getGeoCodesOfICAO(flight.getOrigin()), destinationGeocodes = geoMapper.getGeoCodesOfICAO(flight.getDestination());
			for(Geocodes airportGeocodes: nearbyAirports.values()){
				if(airportGeocodes.diff(originGeocodes) < Constants.EUCLEDIAN_THRESHOLD
						|| airportGeocodes.diff(destinationGeocodes) < Constants.EUCLEDIAN_THRESHOLD){
					flightsInRange.add(flight);
				}
			}
		}
		return flightsInRange;
	}

	public static int getLevenshteinDistance(String a, String b){
		if(a == null || b == null )
			return Integer.MAX_VALUE;
		return computeLevenshteinDistance(a,b);
	}
	
	private static int minimum(int a, int b, int c) {
		return Math.min(Math.min(a, b), c);
	}

	private static int computeLevenshteinDistance(CharSequence str1,
			CharSequence str2) {
		int[][] distance = new int[str1.length() + 1][str2.length() + 1];

		for (int i = 0; i <= str1.length(); i++)
			distance[i][0] = i;
		for (int j = 1; j <= str2.length(); j++)
			distance[0][j] = j;

		for (int i = 1; i <= str1.length(); i++)
			for (int j = 1; j <= str2.length(); j++)
				distance[i][j] = minimum(
						distance[i - 1][j] + 1,
						distance[i][j - 1] + 1,
						distance[i - 1][j - 1]
								+ ((str1.charAt(i - 1) == str2.charAt(j - 1)) ? 0
										: 1));

		return distance[str1.length()][str2.length()];
	}
}
