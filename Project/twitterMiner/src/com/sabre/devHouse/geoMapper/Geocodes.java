package com.sabre.devHouse.geoMapper;

public class Geocodes {
	private double latitude;
	private double longitude;
	public Geocodes(double latitude, double longitude){
		this.latitude = latitude;
		this.longitude = longitude;
	}
	public double getLan(){
		return latitude;
	}
	public double getLon(){
		return longitude;
	}
	public String toString(){
		return "[" + latitude + " , " + longitude + "]";
	}
	public double diff(Geocodes geocode){
		if (geocode == null){
			return Double.MAX_VALUE;
		}
		return Math.sqrt(Math.pow(geocode.getLan()-latitude,2) + Math.pow(geocode.getLon()-longitude,2));
	}
}
