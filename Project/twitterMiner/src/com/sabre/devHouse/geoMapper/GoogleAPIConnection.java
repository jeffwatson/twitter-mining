package com.sabre.devHouse.geoMapper;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.sabre.devHouse.util.Constants;

public class GoogleAPIConnection extends GeoConnection implements GeocodesMapper{
	public static void main (String [] args){
		GoogleAPIConnection tmp = new GoogleAPIConnection();
		tmp.getAirportsNear(new Geocodes(32.982622,-97.160447));
	}

	public GoogleAPIConnection(){
		super();
	}

	private String makeRequest(String baseUrl, Map<String, String> params){
		StringBuilder fullUrlBuff = new StringBuilder(baseUrl);
		Set<String> keys = params.keySet();
		for(String key: keys){
			fullUrlBuff.append(key);
			fullUrlBuff.append("=");
			fullUrlBuff.append(params.get(key));
			fullUrlBuff.append("&");
		}
		String fullUrl = fullUrlBuff.toString();
		return fullUrl = fullUrl.substring(0, fullUrl.length()-1);
	}

	public String sendGetRequest(String request){
		URLConnection connection = null;
		InputStream input = null;
		byte [] buffer = new byte[8000];
		int readBytes;
		StringBuffer toReturn = new StringBuffer();
		try {
			URL urlObj = new URL(request);
			Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(Constants.PROXY, Constants.PROXY_PORT));
			connection = urlObj.openConnection(proxy);
			input = connection.getInputStream();
			while ((readBytes = input.read(buffer, 0, buffer.length)) != -1) {
				toReturn.append(new String(buffer, 0, readBytes));
			}
			System.out.flush();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != input) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return toReturn.toString();
	}
	@Override
	public Map<String, Geocodes> getAirportsNear(Geocodes geoCodes) {
		Map<String, Geocodes> airports = new HashMap<String, Geocodes>();
		Map<String, String> params= new HashMap<String, String>();
		params.put("location", geoCodes.getLan()+","+geoCodes.getLon());
		params.put("rankby", "distance");
		params.put("types", "airport");
		params.put("sensor", "false");
		params.put("keyword", "airport");
		params.put("key", "AIzaSyAa0fsJdO4HDWiI03XzC5dFGz8VYfkkBBg");
		String url = makeRequest("https://maps.googleapis.com/maps/api/place/nearbysearch/xml?", params);
		String response = sendGetRequest(url);

		try{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			InputSource is = new InputSource(new StringReader(response));
			Document doc = builder.parse(is);   
			doc.getDocumentElement().normalize();
			NodeList nList = doc.getElementsByTagName("result");
			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				Element eElement = (Element) nNode;
				String airport = eElement.getElementsByTagName("name").item(0).getTextContent();
				eElement = (Element)eElement.getElementsByTagName("geometry").item(0);
				eElement = (Element)eElement.getElementsByTagName("location").item(0);
				double lan = Double.parseDouble(eElement.getElementsByTagName("lat").item(0).getTextContent());
				double lon = Double.parseDouble(eElement.getElementsByTagName("lng").item(0).getTextContent());
				Geocodes geocodes = new Geocodes(lan, lon);
				airports.put(airport, geocodes);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return airports;
	}
}
