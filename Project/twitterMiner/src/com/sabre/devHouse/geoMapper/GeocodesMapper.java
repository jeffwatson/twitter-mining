package com.sabre.devHouse.geoMapper;

import java.util.Map;

public interface GeocodesMapper {
	public Map<String,Geocodes> getAirportsNear(Geocodes geoCodes);
	public Geocodes getGeoCodesOfICAO(String ICAO);
}
