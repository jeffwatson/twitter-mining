package com.sabre.devHouse.geoMapper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sabre.devHouse.util.Constants;
import com.sabre.devHouse.util.ParserCSV;

public class GeoConnection {
	Map<String, Geocodes> mapICAOtoGeocodes = new HashMap<String, Geocodes>();
    public static URL AirportsRes;
	public GeoConnection(){
		ParserCSV parser = new ParserCSV();
		BufferedReader input;
		try{
            AirportsRes = getClass().getClassLoader().getResource(Constants.AIRPORT_DATA_FILE_PATH);
            File f = new File(AirportsRes.getFile());

			input = new BufferedReader(new FileReader(f));
			String line;
			while((line = input.readLine()) != null){
				List<String> elems = parser.parseCommaVSLine(line);
				mapICAOtoGeocodes.put(elems.get(5), new Geocodes(Double.parseDouble(elems.get(6)), Double.parseDouble(elems.get(7))));

			}
			input.close();
		}catch(IOException ioe){
			ioe.printStackTrace();
		} 
	}

	public Geocodes getGeoCodesOfICAO(String ICAO) {
		return mapICAOtoGeocodes.get(ICAO);
	}

	/*public static void main (String [] args){
		GeoConnection tmp = new GeoConnection();
		System.out.println(tmp.getGeoCodesOfICAO("KDFW"));
	}*/
}
