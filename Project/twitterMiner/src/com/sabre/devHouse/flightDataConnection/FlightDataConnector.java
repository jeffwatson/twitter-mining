package com.sabre.devHouse.flightDataConnection;

import java.util.Date;
import java.util.List;

public interface FlightDataConnector {
    public boolean getPossibleFlights(Date date, List<FlightLeg> list);

}
