package com.sabre.devHouse.flightDataConnection;


/**
 * Package: com.sabre.devHouse.twitterMiner
 * Project: twitter-mining
 * <p/>
 * Author: Jeff Watson <Jeff.Watson@sabre.com>
 * Date: 7/15/13
 * Time: 2:02 PM
 */
public class FlightLeg {

    private String Ident;
    private String Type;
    private String Origin;
    private String OriginName;
    private String OriginCity;
    private String Destination;
    private String DestinationName;
    private String DestinationCity;
    private String DepartureTime;
    private String ArrivalTime;
    private String Enroute;
    private int Id;

    /*public FlightLeg() {
        try{
            factory = new Configuration().configure().buildSessionFactory();
        }catch (Throwable ex) {
            System.err.println("Failed to create sessionFactory object." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }   */

    public FlightLeg() {}


    public int getId() {
        return Id;
    }

    public void setId(int i) {
        this.Id = i;
    }

    public String getIdent() {
        return Ident;
    }

    public void setIdent(String ident) {
        Ident = ident;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getOrigin() {
        return Origin;
    }

    public void setOrigin(String origin) {
        Origin = origin;
    }

    public String getOriginName() {
        return OriginName;
    }

    public void setOriginName(String originName) {
        OriginName = originName;
    }

    public String getOriginCity() {
        return OriginCity;
    }

    public void setOriginCity(String originCity) {
        OriginCity = originCity;
    }

    public String getDestination() {
        return Destination;
    }

    public void setDestination(String destination) {
        Destination = destination;
    }

    public String getDestinationName() {
        return DestinationName;
    }

    public void setDestinationName(String destinationName) {
        DestinationName = destinationName;
    }

    public String getDestinationCity() {
        return DestinationCity;
    }

    public void setDestinationCity(String destinationCity) {
        DestinationCity = destinationCity;
    }

    public String getDepartureTime() {
        return DepartureTime;
    }

    public void setDepartureTime(String departureTime) {
        DepartureTime = departureTime;
    }

    public String getArrivalTime() {
        return ArrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        ArrivalTime = arrivalTime;
    }

    public String getEnroute() {
        return Enroute;
    }

    public void setEnroute(String enroute) {
        Enroute = enroute;
    }
    
    public String toString(){
    	return "Ident: " + Ident +
    			//" Type: " + Type +
    			" Origin: " + Origin +
    			//" OriginName: " + OriginName +
    			//" OriginCity: " + OriginCity +
    			" Destination: " + Destination +
    			//" DestinationName: " + DestinationName +
    			//" DestinationCity: " + DestinationCity +
    			" DepartureTime: " + DepartureTime +
    			" ArrivalTime: " + ArrivalTime //+
    			//" Enroute: " + Enroute
    			;

    }
}
