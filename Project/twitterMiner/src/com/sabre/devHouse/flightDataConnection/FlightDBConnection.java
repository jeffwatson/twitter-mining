package com.sabre.devHouse.flightDataConnection;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.sabre.devHouse.util.Constants;

/**
 * Package: com.sabre.devHouse.twitterMiner
 * Project: twitter-mining
 * <p/>
 * Author: Jeff Watson <Jeff.Watson@sabre.com>
 * Date: 7/16/13
 * Time: 9:42 AM
 */
public class FlightDBConnection implements FlightDataConnector{
    private static final Logger log = Logger.getRootLogger();

    private static SessionFactory factory;
    private List<FlightLeg> mList;

    public FlightDBConnection(){
        try{
            factory = new Configuration().configure().buildSessionFactory();
        }catch (Throwable ex) {
            log.error("Failed to Create SessionFactory object.", ex);
            throw new ExceptionInInitializerError(ex);
        }
        Session session = factory.openSession();


        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            mList = session.createQuery("FROM com.sabre.devHouse.flightDataConnection.FlightLeg").list();

            tx.commit();
        }catch (Exception e) {
            if (tx!=null) tx.rollback();
            log.error("Failed to commit changes.", e);
        }finally {
            session.close();
        }
    }

    public void listFlights(){
        // for some reason, this always goes one too far, resulting in a null object at the end
        Iterator iterator = mList.iterator();
        do {
            FlightLeg flight = (FlightLeg) iterator.next();
            System.out.print("ident: " + flight.getIdent());
            System.out.print(", Type: " + flight.getType());
            System.out.print(", Origin: " + flight.getOrigin());
            System.out.print(", OriginName: " + flight.getOriginName());
            System.out.print(", OriginCity: " + flight.getOriginCity());
            System.out.print(", Destination: " + flight.getDestination());
            System.out.print(", DestinationName: " + flight.getDestinationName());
            System.out.print(", DestinationCity: " + flight.getDestinationCity());
            System.out.print(", Departure: " + flight.getDepartureTime());
            System.out.print(", Arrival: " + flight.getArrivalTime());
            System.out.print(", Enroute: " + flight.getEnroute()+ "\n\n");
        } while ( iterator.hasNext());
    }

    @Override
    public boolean getPossibleFlights(Date ts, List<FlightLeg> list){

        // TODO format the timestamp coming from twitter...
        //String date = ts.toString();

        Long timestamp;
        Long arrivalTime;
        Long departureTime;
        SimpleDateFormat flightDB_sdf = new SimpleDateFormat("M/dd/yy H:mm");

        String date = flightDB_sdf.format(ts);

        try {
            timestamp = flightDB_sdf.parse(date).getTime();

            Iterator iterator = mList.iterator();
            while(iterator.hasNext()) {
                FlightLeg flight = (FlightLeg) iterator.next();
                if(flight != null) {

                    try {
                        // Transform the date into a long for maths
                        if(flight.getArrivalTime() != null || flight.getDepartureTime() != null) {
                            departureTime = flightDB_sdf.parse(flight.getDepartureTime()).getTime();
                            arrivalTime = flightDB_sdf.parse(flight.getArrivalTime()).getTime();

                            // apply the buffers
                            departureTime -= Constants.TIME_OF_TWEET_THRESHOLD;
                            arrivalTime += Constants.TIME_OF_TWEET_THRESHOLD;

                            if(timestamp >= departureTime && timestamp <= arrivalTime) {
                                log.info("Adding flight " + flight.getId() + " to list.");
                                list.add(flight);
                            }
                        }
                    } catch (ParseException e) {
                        log.error("Unable to parse table row.", e);
                    }
                }
            }
        }
        catch (ParseException e) {
            log.error("Unable to parse input time.", e);
        }

        return list.size() == 1;
    }
}

