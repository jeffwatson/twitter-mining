<%@ page import="com.springapp.mvc.InterfaceController" %>
<%@ page import="com.springapp.mvc.ChartDataConnection" %>

<%
    ChartDataConnection dbConnect = new ChartDataConnection();
%>

<html lang="en">
<head>
    <meta charset="utf-32">
    <title>Twitter Data Mining</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="/assets/css/bootstrap.css" rel="stylesheet">
    <link href="/assets/css/bootstrap-modal.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/font-awesome/css/font-awesome.min.css">
    <link href="/assets/css/common.css" rel="stylesheet">
    <link href="/assets/css/american-airlines.css" rel="stylesheet">
    <style type="text/css">
        body {
            padding-top: 60px;
            padding-bottom: 40px;
        }

        .sidebar-nav {
            padding: 9px 0;
        }

        @media (max-width: 980px) {
            /* Enable use of floated navbar text */
            .navbar-text.pull-right {
                float: none;
                padding-left: 5px;
                padding-right: 5px;
            }
        }
    </style>
    <link href="/assets/css/bootstrap-responsive.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="/assets/js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="/assets/img/favicon.ico.png">


    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
    <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript">

        google.load('visualization', '1', {'packages': ['corechart', 'geochart']});
        google.setOnLoadCallback(drawChart);

        function drawChart() {
            <%--console.log("about to open file");--%>
            <%--//            var json = $.ajax({--%>
            <%--//                url: 'get_piechart_data.php', // make this url point to the data file--%>
            <%--//                dataType: 'json',--%>
            <%--//                async: false--%>
            <%--//            }).responseText;--%>
            <%--//console.log("after file open");--%>
            <%--// Create our data table out of JSON data loaded from server.--%>
            <%--//$('#rawChartData').text()--%>
            <%--var data = new google.visualization.DataTable(<%=ChartDataConnection.loadRawData("resources/charts.json")%>);--%>
            <%--var options = {--%>
            <%--title: 'Sentiment Prevalance (American Airlines)',--%>
            <%--is3D: 'true',--%>
            <%--width: 600,--%>
            <%--height: 400--%>
            <%--};--%>
            <%--// Instantiate and draw our chart, passing in some options.--%>
            <%--//do not forget to check ur div ID--%>
            <%--var chart = new google.visualization.PieChart(document.getElementById('charts1'));--%>
            <%--chart.draw(data, options);--%>

            <%--drawMarkersMap();--%>
        }
        function drawMarkersMap() {
//            var json = $.ajax({
//                url: 'get_geochart_data.php', // make this url point to the data file
//                dataType: 'json',
//                async: false
//            }).responseText;

            // Create our data table out of JSON data loaded from server.
            <%--var data = new google.visualization.DataTable(<%=ChartDataConnection.loadRawData("resources/charts-map.json")%>);--%>
            <%--var options = {--%>
                <%--region: 'US',--%>
                <%--displayMode: 'markers',--%>
                <%--colorAxis: {colors: ['green', 'blue']}--%>
            <%--};--%>
//            var data = new google.visualization.DataTable();
//            data.addColumn('string', 'Country');
//            data.addColumn('number', 'Value');
//            data.addColumn({type:'string', role:'tooltip'});
//            data.addRows([[{v:"Philadelphia, PA",f:"Philadelphia"},1,"Where I live"]]);
//            data.addRows([[{v:"San Francsico, CA",f:"San Francsico"},1,"Where I'm moving to"]]);
//
//            var options = {
//                region: 'US',
//                displayMode: 'markers',
//                resolution: 'provinces',
//                legend: 'none',
//                enableRegionInteractivity: 'true',
//                sizeAxis: {minSize:5,  maxSize: 5},
//                colorAxis: {minValue: 1, maxValue:1,  colors: ['#B92B3D'],
//                    axes: [
//                        {
//                            type: 'Numeric',
//                            position: 'left',
//                            fields: ['value'],
//                            grid: true,
//                            minimum: 0
//                        },
//                        {
//                            type: 'Category',
//                            position: 'bottom',
//                            fields: ['date']
//                        }
//                    ]}};
//            // Instantiate and draw our chart, passing in some options.
//            //do not forget to check ur div ID
//            var chart = new google.visualization.GeoChart(document.getElementById('charts2'));
//            chart.draw(data, options);
        }
    </script>
</head>

<body>
<%
    InterfaceController myInterface = new InterfaceController();
    String pageName = myInterface.getData();
%>
<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container-fluid">
            <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a id="btnHome" class="brand" href="#"><%=pageName%></a>
            <div class="nav-collapse collapse">
                <p class="navbar-text pull-right">
                    <a href="#" class="navbar-link">American Airlines</a>
                </p>
                <ul class="nav">
                    <li id="btnOverview" class=""><a href="#overview">Overview</a></li>
                    <li id="btnFeedback" class=""><a href="#feedback">Feedback</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </div>
</div>

<div class="content-container">
    <div class="content"></div><!--/.content-->
    <div class="text-center">
        <div id="chart-container" class="chart-container hide">
            <div class="container2">
                <a id="btnSentiment" class="btn btn-primary btn-small span1">Sentiment</a>
                <a id="btnOrigin" class="btn btn-primary btn-small span1">Origin</a>
                <a id="btnDestination" class="btn btn-primary btn-small span1">Destination</a>
            </div>
            <div id="charts1" class="chart">
                <img src='<%=ChartDataConnection.drawPieChart()%>'/>
            </div>
            <div id="charts2" class="chart">
                <img src='<%=ChartDataConnection.drawPlotChart()%>'/>
            </div>
        </div>
    </div>
</div>

<footer>
    <p>&copy; Sabre Holdings 2013</p>
    <p>Powered by:</p>
    <a href="https://code.google.com/apis/console/"><img src="assets/img/google.png" class="footer-img"/></a>
    <a href="http://www.alchemyapi.com/"><img src="assets/img/alchemy.png" class="footer-img"/></a>
</footer>

<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

<script type="text/JavaScript" src="/assets/js/index.js"></script>

<script src="/assets/js/bootstrap-modal.js"></script>
<script src="/assets/js/bootstrap-modalmanager.js"></script>

</body>
</html>
