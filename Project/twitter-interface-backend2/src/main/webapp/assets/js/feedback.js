//chrome.exe --allow-file-access-from-files
$(document).ready(function() {
    $('a[id*=feedback]').click(function() {
        $('#currentFeedbackID').text($(this).find('#feedbackID').text());
        //alert($('#currentFeedbackID').text());

        var pictureSelector = "#feedbackPicture" + $('#currentFeedbackID').text();
        $('#detailPicture').attr('src', $(pictureSelector).text());

        var userSelector = "#feedbackUser" + $('#currentFeedbackID').text();
        $('#detailUser').text($(userSelector).text());

        var postedDateSelector = "#feedbackPostedDate" + $('#currentFeedbackID').text();
        $('#detailPostedDate').text($(postedDateSelector).text());

        var textSelector = "#feedbackText" + $('#currentFeedbackID').text();
        $('#detailText').text($(textSelector).text());

        var sentimentSelector = "#feedbackSentiment" + $('#currentFeedbackID').text();
        $('#detailSentiment').text(($(sentimentSelector).text() * 100) + "%");

        var derivedSentimentSelector = "#feedbackDerivedSentiment" + $('#currentFeedbackID').text();
        $('#detailDerivedSentiment').text($(derivedSentimentSelector).text());

        var originSelector = "#feedbackOrigin" + $('#currentFeedbackID').text();
        $('#detailOrigin').text($(originSelector).text());

        var destinationSelector = "#feedbackDestination" + $('#currentFeedbackID').text();
        $('#detailDestination').text($(destinationSelector).text());

        var retweetCountSelector = "#feedbackRetweetCount" + $('#currentFeedbackID').text();
        $('#detailRetweetCount').text($(retweetCountSelector).text());
    });
    function parseDate(from){
        from = from.replace('T', '').replace(/-/g,'/');
        return new Date(from);
    }
    $('#btnFilter').click(function() {
        console.log('filtering');
        var count = 0;
        console.log("Feedback Count: " + $('#currentFeedbackCount').text());

        //use subtractive filtering, as in make all visible and remove from the list as the filters are processed this
        //  prevents incorrect overlapping.
        for (var i = 0; i < $('#currentFeedbackCount').text(); i++) {
            var derivedSentimentSelector = "#feedbackDerivedSentiment" + i;
            $(derivedSentimentSelector).parent().parent().attr('class','');
        }
        for (var i = 0; i < $('#currentFeedbackCount').text(); i++) {
            if ($('#filterSentiment').find(":selected").text() != "Select All") {
                console.log("PERFORMING SENTIMENT FILTER");
                var derivedSentimentSelector = "#feedbackDerivedSentiment" + i;
                console.log("Selector: " + derivedSentimentSelector);
                console.log("Comparing: " + $(derivedSentimentSelector).text().toUpperCase() + " to " + $('#filterSentiment').find(":selected").text().toUpperCase());
                if ($(derivedSentimentSelector).text().toUpperCase().indexOf($('#filterSentiment').find(":selected").text().toUpperCase()) > -1) {
                    console.log("match");
                    count++;
                }
                else {
                    console.log("mismatch");
                    $(derivedSentimentSelector).parent().parent().attr('class','hide');
                }
            }
            if ($('#filterDateFrom').val() != "" || $('#filterDateTo').val() != "") {
                console.log("PERFORMING DATE FILTER");
                var dateSelector = "#feedbackPostedDate" + i;
                console.log("Selector: " + dateSelector);
                var feedbackDate = parseDate($(dateSelector).text());
                var filterDateFrom;
                if ($('#filterDateFrom').val() != "") {
                    filterDateFrom = parseDate($('#filterDateFrom').val());
                } else { filterDateFrom = new Date("01/01/1800") }  //if no beginning date, set it to way in the past
                var filterDateTo;
                if ($('#filterDateTo').val() != "") {
                    filterDateTo = parseDate($('#filterDateTo').val());
                } else { filterDateTo = new Date("01/01/2999") }  //if no end date, set it to way in the future
                console.log("Comparing: " + feedbackDate + " to " + filterDateFrom + " and " + filterDateTo);
                if (feedbackDate >= filterDateFrom && feedbackDate <= filterDateTo) {
                    console.log("match");
                    count++;
                }
                else {
                    console.log("mismatch");
                    $(dateSelector).parent().parent().attr('class','hide');
                }
            }
            if ($('#filterOrigin').val() != "") {
                console.log("PERFORMING ORIGIN FILTER");
                var originSelector = "#feedbackOrigin" + i;
                console.log("Selector: " + originSelector);
                console.log("Comparing: " + $(originSelector).text().toUpperCase() + " to " + $('#filterOrigin').val().toUpperCase());
                if ($(originSelector).text().toUpperCase().indexOf($('#filterOrigin').val().toUpperCase()) > -1) {
                    console.log("match");
                    count++;
                }
                else {
                    console.log("mismatch");
                    $(originSelector).parent().parent().attr('class','hide');
                }
            }
            if ($('#filterDestination').val() != "") {
                console.log("PERFORMING DESTINATION FILTER");
                var destinationSelector = "#feedbackDestination" + i;
                console.log("Selector: " + destinationSelector);
                console.log("Comparing: " + $(destinationSelector).text().toUpperCase() + " to " + $('#filterDestination').val().toUpperCase());
                if ($(destinationSelector).text().toUpperCase().indexOf($('#filterDestination').val().toUpperCase()) > -1) {
                    console.log("match");
                    count++;
                }
                else {
                    console.log("mismatch");
                    $(destinationSelector).parent().parent().attr('class','hide');
                }
            }
        }
        //$('#currentFeedbackCount').text = count;  this is actually the max, not needed
    });
});