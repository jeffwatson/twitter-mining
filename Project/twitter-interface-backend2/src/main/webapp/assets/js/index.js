//chrome.exe --allow-file-access-from-files
$(document).ready(function() {
	//loadContentOverview();
	$('#btnHome').click(function() {
		$('#btnOverview').attr('class','');
		$('#btnFeedback').attr('class','');
        $('#chart-container').attr('class','chart-container hide');
		//loadContentHome();
	});
	$('#btnOverview').click(function() {
		$(this).attr('class','active');
		$('#btnFeedback').attr('class','');
        $('#chart-container').attr('class','chart-container');
		loadContentOverview();
	});
	$('#btnFeedback').click(function() {
		$(this).attr('class','active');
		$('#btnOverview').attr('class','');
        $('#chart-container').attr('class','chart-container hide');
		loadContentFeedback();
	});
});

function loadContentHome() {
    console.log("Index.js: load nothing");
    $('.content').load();
}

function loadContentOverview() {
	console.log("Index.js: load overview.jsp");
	$('.content').load("assets/pages/overview.jsp");
}

function loadContentFeedback() {
	console.log("Index.js: load feedback.jsp");
	$('.content').load("assets/pages/feedback.jsp");
}