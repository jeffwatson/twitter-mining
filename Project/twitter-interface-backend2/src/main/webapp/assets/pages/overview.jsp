<script type="text/javascript">
    $('#chart-container').attr('class','chart-container');
</script>
<div class="contentOverview">
    <div class="container-fluid">
        <div class="span7 hero-unit hide">
            <div class="container2">
                <p><a href="#" class="btn btn-primary btn-larger span2">Negative Comments &raquo;</a></p>
            </div>
            <div class="container2">
                <p><a href="#" class="btn btn-primary btn-larger span2">Positive Comments &raquo;</a></p>
            </div>
        </div>
    </div>
</div>