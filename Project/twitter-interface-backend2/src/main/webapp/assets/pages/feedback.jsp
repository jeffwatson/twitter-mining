<%@ page import="com.springapp.mvc.FeedbackDetail" %>
<%@ page import="com.springapp.mvc.ChartDataConnection" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%
    //FeedbackDetail detail[] = new FeedbackDetail[3];
    List<FeedbackDetail> detail =  new ArrayList<FeedbackDetail>();
    detail = ChartDataConnection.loadData();
//    for (int i = 0; i < 3; i++) {
//        detail.get(i) = ChartDataConnection.loadData("resources/tweets.json");
//    }
//    for (int i = 0; i < detail.size(); i++) {
//        if (detail.get(i).getText() == request.getParameter("text")) {
//
//        }
//    }
//    System.out.println("refresh");
%>
<script type="text/javascript">
    $('#chart-container').attr('class','chart-container hide');
</script>

<script type="text/JavaScript" src="/assets/js/feedback.js"></script>
<div class="container-fluid">
	<div>
		<div class="span3">
			<div class="well sidebar-nav">
				<div class="filter">
					<span class="header">FILTERS</span>
				</div>
				<div class="filter">
					<label for="filterSentiment" class="control-label cell">Sentiment</label>
					<div class="input">
                        <select id="filterSentiment" name="Sentiment">
                            <option value="SelectAll">Select All</option>
                            <option value="SlightlyPositive">Neutral</option>
                            <option value="SlightlyPositive">Slightly Positive</option>
                            <option value="Satisfied">Satisfied</option>
                            <option value="Positive">Positive</option>
                            <option value="Excited">Excited</option>
                            <option value="SlightlyNegative">Slightly Negative</option>
                            <option value="Dissatisfied">Dissatisfied</option>
                            <option value="Negative">Negative</option>
                            <option value="Angry">Angry</option>
                        </select>
					</div>
				</div>
				<div class="filter">
					<label for="filterDateFrom" class="control-label date-range cell">Date Range</label>
					<div class="input date-range">
						<input id="filterDateFrom" placeholder="MM/DD/YY" type="text" class="cell" />
						<span>to</span>
						<input id="filterDateTo" placeholder="MM/DD/YY" type="text" />
					</div>
				</div>
				<div class="filter">
					<label for="filterOrigin" class="control-label cell">Origin</label>
					<div class="input">
						<input id="filterOrigin" type="text" class="cell" />
					</div>
				</div>
                <div class="filter">
                    <label for="filterDestination" class="control-label cell">Destination</label>
                    <div class="input">
                        <input id="filterDestination" type="text" class="cell" />
                    </div>
                </div>
                <div class="container2">
                    <p><a id="btnFilter" class="btn btn-primary btn-large span2">Apply Filter</a></p>
                </div>
			</div>
		</div><!--/.well -->
	</div><!--/span-->
		<div class="span9">
            <hidden id="currentFeedbackID" class="hide">i</hidden>
            <hidden id="currentFeedbackCount" class="hide"><%=detail.size()%></hidden>
			<div id="feedback-container" class="span8 feedback-container">
                <%
                    for (int i = detail.size()-1; i>= 0; i--) {
                %>
                        <a id='<%="feedback" + i%>' href="#viewDetail" role="button" data-toggle="modal">
                            <div class="feedback">
                                <hidden id="feedbackID" class="hide"><%=i%></hidden>
                                <hidden id='<%="feedbackPicture" + i%>' class="hide"><%=detail.get(i).getPictureLink()%></hidden>
                                <hidden id='<%="feedbackUser" + i%>' class="hide"><%=detail.get(i).getUser()%></hidden>
                                <hidden id='<%="feedbackPostedDate" + i%>' class="hide"><%=detail.get(i).getPostedDate()%></hidden>
                                <hidden id='<%="feedbackRetweetCount" + i%>' class="hide"><%=detail.get(i).getRetweetCount()%></hidden>
                                <label>Feedback:</label><text id='<%="feedbackText" + i%>'><%=detail.get(i).getText()%></text>
                                <hidden id='<%="feedbackSentiment" + i%>' class="hide"><%=detail.get(i).getSentiment()%></hidden>
                                <br /><label>Sentiment:</label><text id='<%="feedbackDerivedSentiment" + i%>'><%=detail.get(i).getDerivedSentiment()%></text>
                                <br /><label>Origin:</label><text id='<%="feedbackOrigin" + i%>'><%=detail.get(i).getOrigin()%></text>
                                <br /><label>Destination:</label><text id='<%="feedbackDestination" + i%>'><%=detail.get(i).getDestination()%></text>
                            </div>
                        </a><!--/span-->
                <%
                    }
                %>
			</div><!--/row-->
		</div><!--/span-->
	</div><!--/row-->
</div><!--/.fluid-container-->
<div id="viewDetail" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h3 id="myModalLabel">Feedback Detail</h3>
    </div>
    <div class="modal-body feedback-detail">
        <div class="feedback-detail-profile">
            <div>
                <div class="feedback-detail-picturebox">
                    <img id="detailPicture" src="/assets/img/unknown.jpg">
                </div>
                <div class="inline-block">
                    <h2 id="detailUser">Unknown</h2>
                    <br /><span class="filler">on:</span>
                    <text id="detailPostedDate" class="subheader">Unknown</text>
                </div>
            </div>
        </div>
        <br /><br /><div class="body">
            <label>Feedback:</label><text id="detailText">Unknown</text>
        </div>
        <br /><label>Sentiment:</label><text id="detailSentiment">Unknown</text>
        <br /><label>Derived Sentiment:</label><text id="detailDerivedSentiment">Unknown</text>
        <br /><label>Origin:</label><text id="detailOrigin">Unknown</text>
        <br /><label>Destination:</label><text id="detailDestination">Unknown</text>
        <br /><label>Retweet Count:</label><text id="detailRetweetCount">Unknown</text>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary">Save changes</button>
    </div>
</div>