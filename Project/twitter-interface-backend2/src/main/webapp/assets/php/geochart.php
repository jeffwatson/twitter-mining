<!DOCTYPE html>
<html>
<head>
	<title>Location of tweets</title>
		    <script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
    		<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
   			<script type='text/javascript' src='https://www.google.com/jsapi'></script>
    		<script type='text/javascript'>
     		google.load('visualization', '1', {'packages': ['geochart']});
     		google.setOnLoadCallback(drawMarkersMap);

			function drawMarkersMap() {
				var json = $.ajax({
					url: 'get_geochart_data.php', // make this url point to the data file
					dataType: 'json',
					async: false
				}).responseText;
				
				// Create our data table out of JSON data loaded from server.
				var data = new google.visualization.DataTable(json);
				var options = {
					region: 'US',
			        displayMode: 'markers',
			        colorAxis: {colors: ['green', 'blue']}
				};
				// Instantiate and draw our chart, passing in some options.
				//do not forget to check ur div ID
      			var chart = new google.visualization.GeoChart(document.getElementById('chart_div'));
      			chart.draw(data, options);
			}
		</script>  

</head>

<body>

	  
	<div id="chart_div" style="width: 900px; height: 500px;"></div>
 
 
</body>
</html>