package com.springapp.mvc;

public class FeedbackDetail {
    private String user;
    private String text;
    private String postedDate;
    private double sentiment;
    private String derivedSentiment;
    private String origin;
    private String destination;
    private int retweetCount;
    private String pictureLink;
    private boolean isPositive;

    public FeedbackDetail() {
        user = "Unknown";
        text = "Unknown";
        sentiment = 0;
        postedDate = "01/01/1970";
        derivedSentiment = "Neutral";
        origin = "Unknown";
        destination = "Unknown";
        retweetCount = 0;
        pictureLink = "assets/img/unknown.jpg";
    }

    public FeedbackDetail(String user, String text, String postedDate, double sentiment, String derivedSentiment, String origin, String destination, int retweetCount, String pictureLink) {
        this.user = user;
        this.text = text;
        this.postedDate = postedDate;
        this.sentiment = sentiment;
        this.derivedSentiment = derivedSentiment;
        this.origin = origin;
        this.destination = destination;
        this.retweetCount = retweetCount;
        this.pictureLink = pictureLink;
    }
    public void setUser(String user) {
        if (user == null) {
            user = "Unknown";
        } else { this.user = user; }
    }
    public void setText(String text) {
        if (text == null) {
            text = "Unknown";
        } else { this.text = text; }
    }
    public void setPostedDate(String postedDate) {
        if (postedDate == null) {
            postedDate = "Unknown";
        } else { this.postedDate = postedDate; }
    }
    public void setSentiment(double sentiment) {
        this.sentiment = sentiment;
        setDerivedSentiment(sentiment);
    }
    private void setDerivedSentiment(double sentiment) {
        if (sentiment > 0) {
            isPositive = true;
            if (sentiment < 0.25) {
                derivedSentiment = "Slightly Positive";
            }
            else if (sentiment >= 0.25 && sentiment < 0.5) {
                derivedSentiment = "Positive";
            }
            else if (sentiment >= 0.5) {
                derivedSentiment = "Satisfied";
            }
            else if (sentiment >= 0.75) {
                derivedSentiment = "Excited";
            }
            else {
                derivedSentiment = "Unknown";
            }
        }
        else if (sentiment < 0) {
            isPositive = false;
            if (sentiment > -0.25) {
                derivedSentiment = "Slightly Negative";
            }
            else if (sentiment <= -0.25 && sentiment > -0.5) {
                derivedSentiment = "Negative";
            }
            else if (sentiment <= -0.5 && sentiment > -0.75) {
                derivedSentiment = "Dissatisfied";
            }
            else if (sentiment <= -0.75) {
                derivedSentiment = "Angry";
            }
            else {
                derivedSentiment = "Unknown";
            }
        }
        else if (sentiment == 0) {
            derivedSentiment = "Neutral";
        }
    }
    public void setOrigin(String origin) {
        if (origin == null) {
            origin = "Unknown";
        } else { this.origin = origin; }
    }
    public void setDestination(String destination) {
        if (destination == null) {
            destination = "Unknown";
        } else { this.destination = destination; }
    }
    public void setRetweetCount(int retweetCount) {
        this.retweetCount = retweetCount;
    }
    public void setPictureLink(String pictureLink) {
        if (pictureLink == null) {
            pictureLink = "Unknown";
        } else { this.pictureLink = pictureLink; }
    }
    public String getUser() {
        return user;
    }
    public String getText() {
        return text;
    }
    public String getPostedDate() {
        return postedDate;
    }
    public double getSentiment() {
        return sentiment;
    }
    public String getDerivedSentiment() {
        return derivedSentiment;
    }
    public String getOrigin() {
        return origin;
    }
    public String getDestination() {
        return destination;
    }
    public int getRetweetCount() {
        return retweetCount;
    }
    public String getPictureLink() {
        return pictureLink;
    }
    public boolean isPositive() {
         return isPositive;
    }
}