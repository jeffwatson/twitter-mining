package com.springapp.mvc;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class InterfaceController {
    private static final Logger log = Logger.getRootLogger();

	//@RequestMapping(method = RequestMethod.GET)
    @RequestMapping(value = "/", method = RequestMethod.GET)
	public String printWelcome(ModelMap model) {
		model.addAttribute("message", "Hello world!");
        //System.out.println("testing log");
        //log.info("This is your message");
		return "hello";
	}


    public String getData() {
        return "Sentiment";
    }
}