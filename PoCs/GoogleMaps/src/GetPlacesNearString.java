import java.util.HashMap;
import java.util.Map;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.util.Set;


public class GetPlacesNearString {
	// URL TO CREATE https://maps.googleapis.com/maps/api/place/nearbysearch/xml?location=LATITUDE,LONGITUDE&rankby=distance&types=airport&sensor=false&key=*****************************

	public static void main (String [] args){
		new GetPlacesNearString();
	}

	/**
	 * This Method returns the latitude and longitude of a string
	 * @param location - textual representation of a location
	 * @return Map containing latitude and longitude of @param location
	 */
	public Map<String, String> getLatLon(String location) {
		Map <String, String> latLon = new HashMap<String, String>();
		String lat = null;
		String lon = null;

		// Use Google Place API to find the lat and lon


		latLon.put("lat" , lat);
		latLon.put("long", lon);

		return latLon;
	}
	
	public GetPlacesNearString(){
		Map<String, String> params= new HashMap<String, String>();
		params.put("location", "32.982622,-97.160447");
		params.put("rankby", "distance");
		params.put("types", "airport");
		params.put("sensor", "false");
		params.put("key", "AIzaSyAa0fsJdO4HDWiI03XzC5dFGz8VYfkkBBg");
		sendGetRequest("https://maps.googleapis.com/maps/api/place/nearbysearch/xml?", params);
	}
	
	public void sendGetRequest(String url, Map<String, String> params){
		StringBuilder fullUrlBuff = new StringBuilder(url);
		Set<String> keys = params.keySet();
		for(String key: keys){
			fullUrlBuff.append(key);
			fullUrlBuff.append("=");
			fullUrlBuff.append(params.get(key));
			fullUrlBuff.append("&");
		}
		String fullUrl = fullUrlBuff.toString();
		fullUrl = fullUrl.substring(0, fullUrl.length()-1);
		URLConnection connection = null;
		InputStream input = null;
		byte [] buffer = new byte[8000];
		int readBytes;
		try {
			URL urlO = new URL(fullUrl);
			Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("www-ad-proxy.sabre.com", 80));
			connection = urlO.openConnection(proxy);
			input = connection.getInputStream();
			while ((readBytes = input.read(buffer, 0, buffer.length)) != -1) {
				System.out.print(new String(buffer, 0, readBytes));
			}
			System.out.flush();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != input) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}	
