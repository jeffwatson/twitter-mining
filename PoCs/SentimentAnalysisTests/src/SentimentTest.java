import com.alchemyapi.api.AlchemyAPI;
import com.alchemyapi.api.*;

import org.xml.sax.SAXException;
import org.w3c.dom.Document;
import java.io.*;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

class SentimentTest {
	public static void main(String[] args) throws IOException, SAXException,
	ParserConfigurationException, XPathExpressionException {

        //System.setProperty("https.proxyHost", "151.193.220.27");
        //System.setProperty("https.proxyPort", "80");

		AlchemyAPI alchemyObj = AlchemyAPI.GetInstanceFromFile("api_key.txt");
		Document doc;
		System.out.println("TEXT SENTIMENT");
		doc = alchemyObj.TextGetTextSentiment(
				"that i ate AA.");
		System.out.println(getStringFromDocument(doc));
		System.out.println("TEXT SENTIMENT ENDS");

		AlchemyAPI_TargetedSentimentParams sentimentParams = new AlchemyAPI_TargetedSentimentParams();
        sentimentParams.setShowSourceText(true);
        doc = alchemyObj.TextGetTargetedSentiment("This airplane is terrible.", "airplane", sentimentParams);
        System.out.print(getStringFromDocument(doc));// both strings have to be 5 < string.size


        doc = alchemyObj.TextGetRankedNamedEntities(
                "Hello there, my name is Bob Jones.  I live in the United States of America.  " +
                "Where do you live, Fred?");
            System.out.println(getStringFromDocument(doc));

	}
	// utility method
	private static String getStringFromDocument(Document doc) {
		try {
			DOMSource domSource = new DOMSource(doc);
			StringWriter writer = new StringWriter();
			StreamResult result = new StreamResult(writer);

			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.transform(domSource, result);

			return writer.toString();
		} catch (TransformerException ex) {
			ex.printStackTrace();
			return null;
		}
	}

}
